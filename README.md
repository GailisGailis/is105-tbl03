Gjennomgang av klient / server arkitekturen i Golang 

Et eksempel for en klient (fra Golang dokumentasjon):
```golang
conn, err := net.Dial("tcp", "golang.org:80")
if err != nil {
	// handle error
}
fmt.Fprintf(conn, "GET / HTTP/1.0\r\n\r\n")
status, err := bufio.NewReader(conn).ReadString('\n')
// ...
```

Et eksempel for en server (fra Golang dokumentasjon): 
