package main
  
import "net"
import "fmt"

func handler(c net.Conn) {
    bytesFromAClient := make([]byte, 16)
    c.Read(bytesFromAClient)
    fmt.Println("Bytes from av client ", bytesFromAClient)
    c.Write(bytesFromAClient)
    c.Close()
}

func main() {
    l, err := net.Listen("tcp", ":9405")
    if err != nil {
        panic(err)
    }
    for {
        c, err := l.Accept()
        if err != nil {
            continue
        }
        go handler(c)
    }
}
